#!/bin/bash


re () {
    patt=$1
    repl=$(echo $2 | sed 's/\//\\\//g')
    sed -Ei "s/$1/$2/g" $3
}

pkgs=("$@")
for pkg in "${pkgs[@]}"
do
	if [[ "${pkg}" == *":all" ]]; then
		dpkgs+="${pkg} "
	else
		dpkgs+="${pkg}":"${ARCH} "
	fi
done

arm_cross_sources="/etc/apt/sources.list.d/arm-cross-compile-sources.list"
cp -f /etc/apt/sources.list "$arm_cross_sources"
re "(deb?) \[(.*?)\]" "deb [arch=armhf,arm64] " $arm_cross_sources
re "(deb?) \[(.*?)\]" "deb [arch=amd64] " /etc/apt/sources.list



apt-get -o Dir::Cache=${BUILD_DIR} update
set -x
apt-get -o Dir::Cache=${BUILD_DIR} install -y --download-only ${dpkgs} || exit 1
set +x
readarray -t packages < <(ls ./archives/*.deb);
for package in "${packages[@]}"
do
	dpkg-deb -x "${package}" ${BUILD_DIR}
done
[ -d ${BUILD_DIR}/lib ] && cp -r ${BUILD_DIR}/lib/* ${CLICK_LD_LIBRARY_PATH}
[ -d ${BUILD_DIR}/usr/lib ] && cp -r ${BUILD_DIR}/usr/lib/* ${CLICK_LD_LIBRARY_PATH}
[ -d ${CLICK_LD_LIBRARY_PATH}/${ARCH_TRIPLET} ] && { mv ${CLICK_LD_LIBRARY_PATH}/${ARCH_TRIPLET}/* ${CLICK_LD_LIBRARY_PATH};rmdir ${CLICK_LD_LIBRARY_PATH}/${ARCH_TRIPLET}; }
[ -d ${BUILD_DIR}/usr/bin ] && cp -r ${BUILD_DIR}/usr/bin/* ${CLICK_PATH}
[ -d ${BUILD_DIR}/usr/sbin ] && cp -r ${BUILD_DIR}/usr/sbin/* ${CLICK_PATH}
[ -d ${BUILD_DIR}/usr/share ] && cp -r ${BUILD_DIR}/usr/share ${INSTALL_DIR}
bash ./adapt.sh
tree install
pwd

#package specific information for the desktop file
sed -i "s/@name@/VLC Player/" ${INSTALL_DIR}/*.desktop
sed -i "s/@comment@/Play your media/" ${INSTALL_DIR}/*.desktop
sed -i "s/@keywords@/Internet;WWW;Browser;Web;Explorer/" ${INSTALL_DIR}/*.desktop
sed -i "s|@iconpath@|vlc|" ${INSTALL_DIR}/*.desktop
sed -i "s/@categories@/GNOME;GTK;Network;WebBrowser;/" ${INSTALL_DIR}/*.desktop
#a short description for the package
sed -i "s/@description@/vlc is a mediaplayer/" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package

#!/bin/sh
pkg="$1"
mkdir -p ${CLICK_LD_LIBRARY_PATH} ${CLICK_PATH} ${INSTALL_DIR} ${BUILD_DIR}/archives/partial
cp pkg.desktop ${BUILD_DIR}/${pkg}.desktop
cp pkg.d/* ./build.sh ./apparmor.json ./manifest.json ${BUILD_DIR} || exit 1
sed -i "s/@pkg@/${pkg}/" ${BUILD_DIR}/manifest.json
sed -i "s/@pkg@/${pkg}/" ${BUILD_DIR}/${pkg}.desktop
cp ${BUILD_DIR}/${pkg}.desktop ${BUILD_DIR}/*.json ${INSTALL_DIR}
cp ${BUILD_DIR}/pkg.sh ${CLICK_PATH}/${pkg}.sh
